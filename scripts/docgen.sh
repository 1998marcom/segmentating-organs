#!/usr/bin/env bash

pydoctor --make-html --html-output=docs --project-name=segmentating-organs src
