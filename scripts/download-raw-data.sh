#!/usr/bin/env bash

DATA_DIR=$(dirname "$0")/../data/

# Download the data from kaggle: kaggle terminal API required to be installed
# and properly configured, kaggle competitions rules accepted via web GUI
kaggle competitions download -p $DATA_DIR hubmap-organ-segmentation
kaggle competitions download -p $DATA_DIR hubmap-kidney-segmentation 
# If you have a feeling that the download didn't go well, you may force a
# re-download by adding `force`, like:
# kaggle competitions download --force -p $DATA_DIR hubmap-organ-segmentation

# Make folder
mkdir $DATA_DIR/hubmap-organ-segmentation
mkdir $DATA_DIR/hubmap-kidney-segmentation

# Unzip data
unzip $DATA_DIR/hubmap-organ-segmentation.zip -d $DATA_DIR/hubmap-organ-segmentation
unzip $DATA_DIR/hubmap-kidney-segmentation.zip -d $DATA_DIR/hubmap-kidney-segmentation

# Remove zipped data archives
rm $DATA_DIR/hubmap-organ-segmentation.zip $DATA_DIR/hubmap-kidney-segmentation.zip


