#!/usr/bin/env bash

# We are defaulting to the hubmap-kidney-segmentation directory structure, which
# is:
# hubmap-kidney-segmentation
#  ├── HuBMAP-20-dataset_information.csv
#  ├── sample_submission.csv
#  ├── test
#  │   └── 00fadf.tiff
#  ├── train
#  │   ├── d3fadf.tiff
#  │   └── d3fadf.json
#  └── train.csv

DATA_DIR=$(dirname "$0")/../data/

# Converting hubmap-organ-segmentation to standard dataset
mv $DATA_DIR/hubmap-organ-segmentation/train_annotations/* $DATA_DIR/hubmap-organ-segmentation/train_images/
mv $DATA_DIR/hubmap-organ-segmentation/train_images $DATA_DIR/hubmap-organ-segmentation/train
mv $DATA_DIR/hubmap-organ-segmentation/test_images $DATA_DIR/hubmap-organ-segmentation/test
rm -r $DATA_DIR/hubmap-organ-segmentation/train_annotations
sed -i 's/rle/encoding/' $DATA_DIR/hubmap-organ-segmentation/train.csv
