<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Segmentating organs](#segmentating-organs)
    - [How to clone this repo](#how-to-clone-this-repo)
    - [Notable use cases](#notable-use-cases)
        - [Full reproducibility: train and test](#full-reproducibility-train-and-test)
        - [Lazy usage: use pretrained models for inference](#lazy-usage-use-pretrained-models-for-inference)
    - [The docs](#the-docs)
    - [Repo structure explained](#repo-structure-explained)
    - [The configuration file (`params.yaml`)](#the-configuration-file-paramsyaml)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Segmentating organs

### How to clone this repo
Note that you should have both `git` and `dvc` (with S3 extensions) installed.
DVC and its S3 extensions might be installed with `pip install dvc[s3]`.

To clone this repo:

```
git clone https://gitlab.com/1998marcom/segmentating-organs
```

You may then use `dvc` to launch the pipeline you need, see [notable use
cases](#notable-use-cases).

### Notable use cases

##### Full reproducibility: train and test
Note that you might need to pull relevant data from remote storages. See `dvc
dag` for the dependency graph. E.g.:
```
dvc pull data/hubmap-organ-segmentation
```

Then to run the full pipeline:
```
dvc repro
```

##### Lazy usage: use pretrained models for inference
Note that this usage requires downloading the pretrained models: `dvc pull
train` or `dvc pull models-archive` depending on which model you are using (see
the testing settings in `params.yaml`).
```
dvc repro test
```
If dvc just says that everything is up to date, you may force-run the test with:
```
python3 src/test.py
```

### The docs
The docs are statically built using `pydoctor` by gitlab's CI. You can find the online version
[here](https://1998marcom.gitlab.io/segmenting-organs/).

You may also access an offline copy with:
```
./scripts/docgen.sh # Calls pydoctor with appropriate config params
firefox docs/index.html
```

##### Dataset directory structure

We require all datasets to conform to the directory structure of the
hubmap-kidney-segmentation dataset.

```
hubmap-kidney-segmentation
├─HuBMAP-20-dataset_information.csv
├─sample_submission.csv
├─test
│ └─00fadf.tiff
├─train
│ ├─d3fadf.tiff
│ └─d3fadf.json
└─train.csv
```

### Repo structure explained
```
segmentating-organs
├─.dvc
├─data
│ ├─hubmap-organ-segmentation
│ ╰─hubmap-kidney-segmentation
├─live_results
│ ├─train
│ │ ├─scalars
│ │ ╰─report.html
│ ╰─train.json
├─scripts
│ ├─download-raw-data.sh
│ ├─docgen.sh
│ ╰─format_hubmap-organ-segmentation.sh
├─src
│ ├─dataset.py
│ ├─model.py
│ ├─submit.py
│ ├─train.py
│ ├─test.py
│ ╰─use.py
├─README.md
├─params.yaml
╰─requirements.txt
```

* `.dvc` is a directory used by dvc to store its data. It's only partially under
  version control.
* `data` contains the datasets. Data are under dvc-git version control.
* `live_results` contains the results from the latest pipeline. The
  `live_results/train/report.html` report is particularly handy.
* `scripts` contains useful scripts for downloading data
  (`download-raw-data.sh`), formatting them in a defined directory structure 
  (`format_hubmap-organ-segmentation`) and generating the docs (`docgen.sh`).
* `src` contains the source codes.
  Further info about this sources can be found in the static docs
  [here](https://1998marcom.gitlab.io/segmenting-organs/).
  It contains codes used for:
  - `dataset.py` loading data into torch's datasets and dataset preprocessing.
  - `model.py` defining a custom network and instancing models.
  - `train.py` training the network and saving the trained models.
  - `test.py` loading the trained network and testing it against the test
	datasets.
* `README.md` is this file.
* `params.yaml` is the main configuration file that dictates how the pipeline
  should behave. See [its section](#the-configuration-file-paramsyaml).
* `requirements.txt` contains the required python dependencies

### The configuration file (`params.yaml`)
TODO
