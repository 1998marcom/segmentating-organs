"""
Trains the chosen network. Configurable via `params.yaml`.
"""

from pathlib import Path
from tqdm import tqdm
import yaml, torch
import os
import numpy as np
import random

# Nothing should be random
torch.manual_seed(42)
torch.cuda.manual_seed_all(42)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
np.random.seed(42)
random.seed(42)
os.environ['PYTHONHASHSEED'] = str(42)

BASE_DIR = Path(__file__).parent.parent
GLOBALS = globals()
PARAMS = yaml.safe_load(open(BASE_DIR/"params.yaml", "r"))

# Import DVCLive
import dvclive
live = dvclive.Live(path=BASE_DIR/"live_results/train")

# Get cpu or gpu device for training.
device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")
torch.set_num_threads(10) 


from model import get_model

# Instanciate model
img_size = (PARAMS['image']['width'], PARAMS['image']['height']) 
model = get_model().to(device)
#print(model)

# Number of trainable parameters
def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)
print("\nNumber of trainable params:", count_parameters(model))

start_from = PARAMS['training'].get('start_from', None)
if start_from:
    model.load_state_dict(torch.load(BASE_DIR/start_from['model']))
start_epoch = -1 if not start_from else start_from['epoch']


from dataset import merge_paths_to_datasets, AugmentedSampler
from torch.utils.data import DataLoader, ConcatDataset

train_dataset, validation_dataset, test_dataset = merge_paths_to_datasets(
    [BASE_DIR/"data/hubmap-organ-segmentation/"], 
    val_size=PARAMS['split']['validation'], test_size=PARAMS['split']['test'],
)
print("Imported datasets")

train_dataloader = DataLoader(
    dataset=train_dataset,
    batch_size=PARAMS['batch_size'],
    sampler=AugmentedSampler(train_dataset, augment_factor=PARAMS['training']['augment_factor']),
)
validation_dataloader = DataLoader(validation_dataset, batch_size=PARAMS['batch_size'])
test_dataloader = DataLoader(test_dataset, batch_size=PARAMS['batch_size'])


import segmentation_models_pytorch as smp
dice_loss = smp.losses.DiceLoss('multiclass', classes=[1]).to(device)

print(f"Getting weights for CrossEntropyLoss: {len(train_dataloader)}its needed")
white_pixels = 0
black_pixels = 0
for batch, (x, y) in tqdm(enumerate(train_dataloader)):
    white_pixels += (y > 0.5*torch.ones(y.size())).sum().item()
    black_pixels += (y <= 0.5*torch.ones(y.size())).sum().item()
    if batch * x.size()[0] > 50:
        print(f"Breaking earlier from loop, enough data available")
        break
total_pixels = len(train_dataloader)*PARAMS['batch_size'] * img_size[0] * img_size[1]
black_pixels = int(total_pixels * black_pixels / (black_pixels+white_pixels))
white_pixels = total_pixels - black_pixels
print(f"total_pixels: {total_pixels}, white: {white_pixels}, black: {black_pixels}")

from torch import nn

crossentropy_loss = nn.CrossEntropyLoss(weight=torch.Tensor([
    total_pixels/black_pixels,
    total_pixels/white_pixels,
])).to(device)

def loss_fn(pred, y, epoch=0, validation=False):
    
    exponent = 2
    lmbda = 4
    lr_decay = PARAMS['training']['learning_rate_decay']
    x = epoch
    A, B, C = 0.45, 0.15, 0.05
    
    #dice_coeff = PARAMS['training']['learning_rate_decay']**(-lmbda*epoch)
    dice_coeff = 1.#(1+epoch)**exponent
    dice_part = dice_loss(pred, y)
    cross_coeff = 1./(1+x)*lr_decay**epoch
    cross_part = crossentropy_loss(pred, y)
    denom = dice_coeff + cross_coeff
    
    loss = (dice_coeff * dice_part + cross_coeff * cross_part) / denom
    return loss

optimizer = torch.optim.SGD(model.parameters(), lr=PARAMS['training']['learning_rate'])
scheduler = torch.optim.lr_scheduler.LambdaLR(
    optimizer,
    lambda epoch: (1+epoch)*PARAMS['training']['learning_rate_decay']**epoch,
    #lambda epoch: 1*PARAMS['training']['learning_rate_decay']**epoch,
)
scheduler.last_epoch = start_epoch
if scheduler.last_epoch >=0:
    scheduler.step()

def train_loop(dataloader, model, loss_fn, optimizer, epoch=1):
    size = len(dataloader)*PARAMS['batch_size']
    model.train() # Enter train mode
    total_pixels = size * model.img_size[0] * model.img_size[1]
    correct_pixels = 0
    total_loss = 0
    for batch, (x, y) in enumerate(dataloader):
        x, y = x.to(device), y.to(device)
        
        # Compute prediction error
        pred = model(x)
        loss = loss_fn(pred, y, epoch=epoch)
        total_loss += loss.item()
        
        oldval = correct_pixels
        #correct_pixels += ((pred+y) > 1.5*torch.ones(pred.size())).sum().item()
        #correct_pixels += ((pred+y) < 0.5*torch.ones(pred.size())).sum().item()
        correct_pixels += (torch.argmax(pred, dim=1) == y).sum().item()
                # Backpropagation
        optimizer.zero_grad() # Force recompute of gradients
        loss.backward()
        optimizer.step()
        
        if batch % 10 == 0:
            loss, current = loss.item(), batch * len(x)
            print(f"loss: {loss:>7f} [{current:>5d}/{size:>5d}]")
            
            ex_x, ex_y, ex_pred = x, y, pred
            for index in range(ex_x.size()[0]):
                e_x = TF.to_pil_image(ex_x[index])
                et_y = torch.as_tensor(ex_y[index], dtype=torch.uint8)
                e_y = ImageOps.autocontrast(TF.to_pil_image(et_y))
                e_pred = ImageOps.autocontrast(TF.to_pil_image(
                    torch.as_tensor(torch.argmax(ex_pred[index],dim=0), dtype=torch.uint8)
                ))
                e_x.save(BASE_DIR/f"examples/train_{batch}_{index}_x.png")
                e_y.save(BASE_DIR/f"examples/train_{batch}_{index}_y.png")
                e_pred.save(BASE_DIR/f"examples/train_{batch}_{index}_pred.png")

    avg_loss = total_loss * PARAMS['batch_size'] / size
    avg_correct = correct_pixels / total_pixels
    return avg_loss, avg_correct


import torchvision.transforms.functional as TF
from PIL import ImageOps

def validation_loop(dataloader, model, loss_fn, show=False, epoch=1):
    size = len(dataloader) * PARAMS['batch_size']
    model.eval() # Enter eval mode
    total_pixels = size * model.img_size[0] * model.img_size[1]
    correct_pixels = 0
    total_loss = 0
    with torch.no_grad():
        for batch, (x, y) in enumerate(dataloader):
            x, y = x.to(device), y.to(device)

            # Compute prediction error
            pred = model(x)
            loss = loss_fn(pred, y, epoch=epoch, validation=True)
            total_loss += loss.item()

            correct_pixels += (torch.argmax(pred, dim=1) == y).sum().item()
            
            if show and batch % 5 == 0:
                ex_x, ex_y, ex_pred = x, y, pred
                for index in range(ex_x.size()[0]):
                    e_x = TF.to_pil_image(ex_x[index])
                    et_y = torch.as_tensor(ex_y[index], dtype=torch.uint8)
                    e_y = ImageOps.autocontrast(TF.to_pil_image(et_y))
                    e_pred = ImageOps.autocontrast(TF.to_pil_image(
                        torch.as_tensor(torch.argmax(ex_pred[index],dim=0), dtype=torch.uint8)
                    ))
                    e_x.save(BASE_DIR/f"examples/val_{batch}_{index}_x.png")
                    e_y.save(BASE_DIR/f"examples/val_{batch}_{index}_y.png")
                    e_pred.save(BASE_DIR/f"examples/val_{batch}_{index}_pred.png")
    
    avg_loss = total_loss * PARAMS['batch_size'] / size
    avg_correct = correct_pixels / total_pixels
    print(f"Validation Error: \n Accuracy: {(100*avg_correct):>0.1f}%, Avg loss: {avg_loss:>8f}\n")
    return avg_loss, avg_correct

# Train with validation
# Calling training
epochs = PARAMS['training']['epochs']
for t in range(start_epoch+1, start_epoch+epochs+1):
    print(f"Epoch {t+1}/{start_epoch+1+epochs}\n--------------------")
    def get_lr(optimizer):
        for param_group in optimizer.param_groups:
            return param_group['lr']
    live.log("learning_rate", get_lr(optimizer))
    train_loss, train_correct = train_loop(train_dataloader, model, loss_fn, optimizer, epoch=t)
    live.log("train/loss", train_loss)
    live.log("train/correct", train_correct)
    val_loss, val_correct = validation_loop(validation_dataloader, model, loss_fn, show=True, epoch=t)
    live.log("validation/loss", val_loss)
    live.log("validation/correct", val_correct)
    live.next_step()
    scheduler.step()

    # Save the model for later use
    (BASE_DIR/PARAMS['training']['models_archive']).mkdir(exist_ok=True)
    torch.save(
        model.state_dict(), 
        BASE_DIR/(PARAMS['training']['models_archive']+f"/UNet_epoch_{t:02d}.pth"),
    )

torch.save(
    model.state_dict(), 
    BASE_DIR/(PARAMS['training']['model']),
)

# Recoverable later with:
#   model = UNet(*args, **kwargs)
#   model.load_state_dict(torch.load(BASE_DIR/"models/UNet.pth"))
#   model.eval()
