"""Tests the chosen network. Configurable via `params.yaml`
"""

from pathlib import Path
from tqdm import tqdm
import yaml, torch

BASE_DIR = Path(__file__).parent.parent
GLOBALS = globals()
PARAMS = yaml.safe_load(open(BASE_DIR/"params.yaml", "r"))


# Get cpu or gpu device for training.
device = "cuda" if torch.cuda.is_available() else "cpu"
print(f"Using {device} device")
torch.set_num_threads(10) 


from model import get_model

# Instanciate model
img_size = (PARAMS['image']['width'], PARAMS['image']['height']) 
model = get_model().to(device)
#print(model)

# Load the trained model
model.load_state_dict(torch.load(BASE_DIR/PARAMS['testing']['model']))
model.eval()

# Number of trainable parameters
def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)
print("\nNumber of trained params:", count_parameters(model))


from dataset import merge_paths_to_datasets, AugmentedSampler
from torch.utils.data import DataLoader, ConcatDataset

# Note that, even if not clear from this line, the internals of the function has a
# fixed state, so that, as long as val_size and test_size do not change, it is
# guaranteed that these dataset are the same of the train.py script
train_dataset, validation_dataset, test_dataset = merge_paths_to_datasets(
    [BASE_DIR/"data/hubmap-organ-segmentation/"], 
    val_size=PARAMS['split']['validation'], test_size=PARAMS['split']['test'],
)
print("Imported datasets")


train_dataloader = DataLoader(
    dataset=train_dataset,
    batch_size=PARAMS['batch_size'],
    sampler=AugmentedSampler(train_dataset, augment_factor=PARAMS['training']['augment_factor']),
)
validation_dataloader = DataLoader(validation_dataset, batch_size=PARAMS['batch_size'])
test_dataloader = DataLoader(test_dataset, batch_size=PARAMS['batch_size'])


import segmentation_models_pytorch as smp
dice_loss = smp.losses.DiceLoss('multiclass', classes=[1]).to(device)

print(f"Getting weights for CrossEntropyLoss: {len(train_dataloader)}its needed")
white_pixels = 0
black_pixels = 0
for batch, (x, y) in tqdm(enumerate(train_dataloader)):
    white_pixels += (y > 0.5*torch.ones(y.size())).sum().item()
    black_pixels += (y <= 0.5*torch.ones(y.size())).sum().item()
    if batch * x.size()[0] > 50:
        print(f"Breaking earlier from loop, enough data available")
        break
total_pixels = len(train_dataloader)*PARAMS['batch_size'] * img_size[0] * img_size[1]
black_pixels = int(total_pixels * black_pixels / (black_pixels+white_pixels))
white_pixels = total_pixels - black_pixels
print(f"total_pixels: {total_pixels}, white: {white_pixels}, black: {black_pixels}")

from torch import nn
import torchvision.transforms.functional as TF
from PIL import ImageOps


crossentropy_loss = nn.CrossEntropyLoss(weight=torch.Tensor([
    total_pixels/black_pixels,
    total_pixels/white_pixels,
])).to(device)

def loss_fn(pred, y):
    return dice_loss(pred, y)

def test_loop(dataloader, model, loss_fn, show=False, showall=False):
    size = len(dataloader.dataset)
    model.eval() # Enter eval mode
    total_pixels = size * model.img_size[0] * model.img_size[1]
    correct_pixels = 0
    total_loss = 0

    correct_white = 0
    pred_white = 0
    y_white = 0
    
    with torch.no_grad():
        for batch, (x, y) in enumerate(dataloader):
            x, y = x.to(device), y.to(device)

            # Compute prediction error
            pred = model(x)
            #loss = loss_fn(pred, y)
            loss = loss_fn(pred, y)
            total_loss += loss.item()

            correct_pixels += (torch.argmax(pred, dim=1) == y).sum().item()
            #correct_pixels += ((pred+y) > 1.5*torch.ones(pred.size())).sum().item()
            #correct_pixels += ((pred+y) < 0.5*torch.ones(pred.size())).sum().item()

            d_pred_white = (torch.argmax(pred, dim=1) > 0.5*torch.ones(y.size()).to(device)).sum().item()
            pred_white += d_pred_white
            d_y_white = (y > 0.5*torch.ones(y.size()).to(device)).sum().item()
            y_white += d_y_white
            d_correct_white = ((torch.argmax(pred, dim=1)+y) > 1.5*torch.ones(y.size()).to(device)).sum().item()
            correct_white += d_correct_white
            
            if batch % 5 == 0 or showall:
                loss, current = loss.item(), batch * len(x)
                print(f"this batch loss: {loss:>7f} [{current:>5d}/{size:>5d}]")
                print(f"this batch Dice coeff: {2*d_correct_white/(d_pred_white+d_y_white)}")

            if (show and batch % 5 == 0) or showall:
                ex_x, ex_y, ex_pred = x, y, pred
                for index in range(ex_x.size()[0]):
                    e_x = TF.to_pil_image(ex_x[index])
                    et_y = torch.as_tensor(ex_y[index], dtype=torch.uint8)
                    e_y = ImageOps.autocontrast(TF.to_pil_image(et_y))
                    e_pred = ImageOps.autocontrast(TF.to_pil_image(
                        torch.as_tensor(torch.argmax(ex_pred[index],dim=0), dtype=torch.uint8)
                    ))
                    nix = batch * PARAMS['batch_size'] + index
                    e_x.save(BASE_DIR/f"examples/{nix}_x.png")
                    e_y.save(BASE_DIR/f"examples/{nix}_y.png")
                    e_pred.save(BASE_DIR/f"examples/{nix}_pred.png")
    
    avg_loss = total_loss * PARAMS['batch_size'] / size
    avg_correct = correct_pixels / total_pixels
    print(f"Test Error: \n Accuracy: {(100*avg_correct):>0.1f}%, Avg loss: {avg_loss:>8f}")
    dice_coefficient = 2 * correct_white / (pred_white + y_white)
    print(f"Dice coefficient: {dice_coefficient}")
    return avg_loss, avg_correct

# Calling testing
test_loss, test_correct = test_loop(test_dataloader, model, dice_loss, showall=True)
