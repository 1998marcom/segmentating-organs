"""
Utilities to retrieve available datasets

As for the data format, we are defaulting to the hubmap-kidney-segmentation 
directory structure:

::

    hubmap-kidney-segmentation
    ├─HuBMAP-20-dataset_information.csv
    ├─sample_submission.csv
    ├─test
    │ ╰─00fadf.tiff
    ├─train
    │ ╰─d3fadf.tiff
    │ ╰─d3fadf.json
    ╰─train.csv
"""

from pathlib import Path
import yaml, torch, re
from time import sleep
import random

BASE_DIR = Path(__file__).parent.parent
GLOBALS = globals()
PARAMS = yaml.safe_load(open(BASE_DIR/"params.yaml", "r"))

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from PIL import Image, ImageOps
from torch.utils.data import (
    DataLoader, Dataset, ConcatDataset, random_split, Sampler,
)
import torchvision.transforms.functional as TF
import cv2
import albumentations as A
from albumentations.pytorch.transforms import ToTensorV2

Image.MAX_IMAGE_PIXELS = None

def string_rle_to_image(width, height, string):
    """Converts run-length encoded mask given as `string` to an image of size
    (width, height)"""
    i = Image.new('1', (width, height))
    i = np.asarray(i, dtype=np.uint8)
    splitstr = string.split()
    encoded_pairs = []
    old_value = None
    for val in splitstr:
        if old_value:
            encoded_pairs.append((old_value, val))
            old_value = None
        else:
            old_value = val
    #encoded_pairs = [m.split() for m in re.findall("[0-9]* [0-9]*", string)]
    for position, length in encoded_pairs:
        position, length = map(int, (position, length))
        position = position - 1 # Some barbarian chose boundaries starting from 1
        #print(position)
        for idx in range(position, position+length):
            row = idx % height
            col = idx // height
            i[row, col] = 1
            #print(row, col)
    #i = Image.fromarray(i)
    #i.show()
    #sleep(5)
    return i

class SegmentationDataset(Dataset):
    """Derived class of torch.utils.data.Dataset"""
    
    def __init__(self, path, img_size, rows=None, suffix=".tiff", transforms=None):
        """Instanciate dataset.

        @arg path: path to dataset, e.g. 'data/hubmap-organ-segmentation'
        @arg img_size: tuple(width, height)
        @arg rows: an optional iterable containing the ids of rows for restricting the dataset to those rows
        @arg suffix: images suffix, e.g. '.tiff' or '.png'
        @arg transforms: an albumentation transform to use for data augmentation
        """
        path = Path(path)
        self.path = path
        df = pd.read_csv(self.path/"train.csv")
        if rows is not None:
            cond = np.sum([df['id'] == row for row in rows], axis=0, dtype=np.bool)
            df = df.loc[cond]
        self.df = df
        self.img_size = img_size
        self.suffix = suffix
        self.transforms = transforms
        #print(self.df.head())

    def __len__(self):
        """Returns length of dataset"""
        return len(self.df)

    def __getitem__(self, idx):
        """Returns the `idx` item from dataset as a tuple (image, mask)"""
        df_row = self.df.iloc[idx]
        stem = str(df_row['id'])
        image_path = self.path/"train"/(stem+self.suffix)
        # Read an image with OpenCV
        image = cv2.imread(str(image_path.resolve()))
        height, width, channels = image.shape
        
        # By default OpenCV uses BGR color space for color images,
        # so we need to convert the image to RGB color space.
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        rlestring = df_row['encoding']
        original_result_im = string_rle_to_image(width, height, rlestring)
        #y_im = ImageOps.pad(original_result_im, self.img_size, color=0, centering=(0.5, 0.5))
        #y = torch.as_tensor(np.array(y_im), dtype=torch.int64)
        #y = TF.to_tensor(y_im)
        transformed = A.Resize(*self.img_size)(image=image, mask=original_result_im)
        x = transformed['image']; y = transformed['mask']
        
        if self.transforms:
            #retensorized = ToTensorV2()(image=np.asarray(x_im), mask=np.asarray(y_im))
            #x = retensorized['image']; y = retensorized['mask']
            transformed = self.transforms(image=x, mask=y)
            x = transformed['image']; y = transformed['mask']
            
        preprocess = A.Compose([
            #A.Normalize(),
            ToTensorV2(),
        ])
        transformed = preprocess(image=x, mask=y)
        x = transformed['image']; y = transformed['mask']
        
        x = x.to(torch.float32)/255
        y = y.to(torch.int64)
        
        return x, y

# Choosing training transforms
training_transforms = A.Compose([
    A.ElasticTransform(alpha=20, sigma=50, alpha_affine=30, p=0.5),
    A.ShiftScaleRotate(shift_limit=0.0625, scale_limit=0.05, rotate_limit=45, p=0.5),
    A.HueSaturationValue(3,10,8),
    A.HorizontalFlip(p=0.5),
    # others to add
])
"""Transforms used for data augmentation while training"""

# We have an eterogeneous dataset, we would like to extract a single, shuffled,
# larger dataset

img_size = (PARAMS['image']['width'], PARAMS['image']['height'])

def path_to_datasets(path, val_size=0.05, test_size=0.9):
    """Given `path` to dataset, returns tuple of (train_set, val_set, test_set)"""
    
    df = pd.read_csv(path/"train.csv")
    indexes = df['id']
    trainval_ids, test_ids = train_test_split(
        indexes, test_size=test_size, 
        random_state=42,
    )
    
    val_size = val_size / (1. - test_size)
    train_ids, val_ids = train_test_split(
        trainval_ids, test_size=val_size,
        random_state=42,
    )
    
    train_dataset = SegmentationDataset(path, img_size, rows=train_ids, transforms=training_transforms)
    val_dataset = SegmentationDataset(path, img_size, rows=val_ids)
    test_dataset = SegmentationDataset(path, img_size, rows=test_ids)
    
    return train_dataset, val_dataset, test_dataset
    
DATASET_PATHS = [BASE_DIR/"data/hubmap-organ-segmentation/"]
"""List of paths to used datasets"""

def merge_paths_to_datasets(paths, val_size=0.2, test_size=0.3):
    """Given `paths`, returns merged tuple of (train_set, val_set, test_set)

    @arg paths: iterable of pathlib.Path or iterable of strings, contains list of 
    paths to merge into a single dataset using path_to_dataset
    @arg val_size: fraction of each dataset to reserve for validation
    @arg test_size: fraction of each dataset to reserve for test
    """
    dataset_matrix = []
    for path in paths:
        #if dataset_matrix.size > 0:
        #    dataset_matrix = np.vstack((dataset_matrix, np.array(path_to_datasets(path))))
        #else:
        dataset_matrix.append(path_to_datasets(path, val_size, test_size))

    train_dataset = ConcatDataset([r[0] for r in dataset_matrix])
    validation_dataset = ConcatDataset([r[1] for r in dataset_matrix])
    test_dataset = ConcatDataset([r[2] for r in dataset_matrix])
    return train_dataset, validation_dataset, test_dataset
    
    
class AugmentedSampler(Sampler):
    """Subclass of torch.utils.data.Sampler
    
    Use together with SegmentationDataset for data augmentation (retrieves all 
    samples from its `data_source`, each one for `augment_factor` times)
    """
    
    def __init__(self, data_source, augment_factor=1):
        self.data_source = data_source
        self.augment_factor = augment_factor
    
    def __len__(self):
        return self.augment_factor * len(self.data_source)
    
    def __iter__(self):
        indexes = list(range(len(self.data_source)))*self.augment_factor
        random.shuffle(indexes)
        return iter(indexes)
