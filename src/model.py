"""
Defines a custom U-Net like network and offers a function to load model based on `params.yaml`
"""
from pathlib import Path
import yaml, torch

BASE_DIR = Path(__file__).parent.parent
GLOBALS = globals()
PARAMS = yaml.safe_load(open(BASE_DIR/"params.yaml", "r"))

from torch import nn
import math


# Define U-net like model
class UNet(nn.Module):
    """
    Customly defined U-Net
    
    In detail, the network is made from an encoders chain and a decoders one.
    Encoders and decoders are defined by the following pseudo-code:
    ::

        # Encoder:
         (0): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
         (1): Conv2d(N, 2N, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=replicate)
         (2): BatchNorm2d(2N, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
         (3): ReLU()
         (4): Conv2d(2N, 2N, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), padding_mode=replicate)
         (5): BatchNorm2d(2N, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
         (6): ReLU()
        # Decoder:
         (0): Conv2d(2N, 2N, kernel_size=(7, 7), stride=(1, 1), padding=(3, 3), padding_mode=replicate)
         (1): BatchNorm2d(2N, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
         (2): ReLU()
         (3): Conv2d(2N, 2N, kernel_size=(5, 5), stride=(1, 1), padding=(2, 2), padding_mode=replicate)
         (4): BatchNorm2d(2N, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
         (5): ReLU()
         (6): PixelShuffle(upscale_factor=2)
         
    A couple of (Conv2d, BatchNorm2d, ReLU) is also applied before the encoders, between encoders and
    decoders at the bottom of the net and at the end of the network, after the decoders.
    The final logits are made by the following pseudo code:
    ::
    
        # Logits maker
         (0): Conv2d(in_channels, out_channels=N_logits=2, kernel_size=1, padding=0),
         (1): Softmax2d(),
    """

    def __init__(
            self, 
            img_size, 
            starting_channels=PARAMS['model'].get('starting_channels', 16),
            delta_to_maximum_depth=PARAMS['model'].get('delta_to_maximum_depth', 4)
            ):
        """
        Instanciate the network
        
        @arg img_size: The size of the input images and of the target masks
        @arg starting_channels: How many channels shall have the internal state after the first
        convolutional layer has been applied to the input image.
        @arg delta_to_maximum_depth: How many more encoder layers would be needed to obtain a
        internal state with 1x1 resolution if starting from the deepest layer.
        """

        super(UNet, self).__init__()
        self.img_size = img_size

        """We are expecting to receive from the dataloader images whose shape
        is (x, y) and x, y must both be a power of two"""

        # `depth` is the "depth" of the UNet, with original image at level zero
        # and the bottom data at level depth. It follows that
        # image_x = bottom_x * (2**depth) and similarly for image_y and bottom_y
        depth = int(math.log2(min(self.img_size)+0.5))-delta_to_maximum_depth 
        self.depth = depth

        # Encoding steps
        self.preencoder = nn.Sequential(
            nn.Conv2d(3, starting_channels, 3, stride=1, padding=1, padding_mode='replicate'),
            nn.BatchNorm2d(starting_channels),
            nn.ReLU(),
            nn.Conv2d(starting_channels, starting_channels, 3, stride=1, padding=1, padding_mode='replicate'),
            nn.BatchNorm2d(starting_channels),
            nn.ReLU(),
        )
        def get_channels_from_index(i):
            """Returns number of channels at depth i"""
            return starting_channels*(2**i)
        for i in range(1, depth):
            setattr(
                self,
                'encoder_{:02d}'.format(i),
                nn.Sequential(
                    nn.MaxPool2d(2),
                    nn.Conv2d(get_channels_from_index(i-1), get_channels_from_index(i), 3, stride=1, padding=1, padding_mode='replicate'),
                    nn.BatchNorm2d(get_channels_from_index(i)),
                    nn.ReLU(),
                    nn.Conv2d(get_channels_from_index(i), get_channels_from_index(i), 3, stride=1, padding=1, padding_mode='replicate'),
                    nn.BatchNorm2d(get_channels_from_index(i)),
                    nn.ReLU(),
                )
            )

        # Bottom end
        BM = 2 # bottom multiplier
        bottom_channels = get_channels_from_index(depth)
        self.bottom_convolution = nn.Sequential(
            nn.Conv2d(get_channels_from_index(depth-1), bottom_channels*BM, 4, stride=2, padding=1, padding_mode='replicate'),
            nn.BatchNorm2d(bottom_channels*BM),
            nn.ReLU(),
        )

        # Decoding steps: note that they are indexed by depth, not by order in which they do apply
        for i in range(1, depth+1):
            setattr(
                self,
                'decoder_{:02d}'.format(i),
                nn.Sequential(
                    nn.Conv2d(get_channels_from_index(i)*BM, get_channels_from_index(i)*BM, 7, padding=3, padding_mode="replicate"),
                    nn.BatchNorm2d(get_channels_from_index(i)*BM),
                    nn.ReLU(),
                    nn.Conv2d(get_channels_from_index(i)*BM, get_channels_from_index(i)*BM, 5, padding=2, padding_mode="replicate"),
                    nn.BatchNorm2d(get_channels_from_index(i)*BM),
                    nn.ReLU(),
                    nn.PixelShuffle(2),
                )
            )

        # Logits maker
        self.logits_maker = nn.Sequential(
            nn.Conv2d(get_channels_from_index(0)*BM, get_channels_from_index(0)*BM, 5, padding=2, padding_mode="replicate"),
            nn.BatchNorm2d(get_channels_from_index(0)*BM),
            nn.ReLU(),
            nn.Conv2d(get_channels_from_index(0)*BM, get_channels_from_index(0)*BM, 5, padding=2, padding_mode="replicate"),
            nn.BatchNorm2d(get_channels_from_index(0)*BM),
            nn.ReLU(),
            nn.Conv2d(get_channels_from_index(0)*BM, 2, 1, padding=0),
            nn.Softmax2d(),
        )

    def forward(self, x):
        """Applies network to image or batch of images. Returns 2xHxW logits mask."""
        #print(f"x.shape: {x.shape}")
        w = x
        #print(f"w.shape: {w.shape}")
        l = [self.preencoder(w)]
        for i in range(1, self.depth):
            #print(f"l[{i}].shape: {l[i].shape}")
            encoder = getattr(self, 'encoder_{:02d}'.format(i))
            l.append(encoder(l[i-1]))

        z = self.bottom_convolution(l[-1])

        for i in range(self.depth, 0, -1):
            # Gets up from depth=i to depth=i-1 and merges ad depth=i-1
            decoder = getattr(self, 'decoder_{:02d}'.format(i))
            z = decoder(z)
            #print(f"current depth, i: {i} \tz.shape: {z.shape} \tl[i-1].shape: {l[i-1].shape}")
            z = torch.cat([z, l[i-1]], dim=z.dim()-3)

        logits = self.logits_maker(z)

        return logits

# Let's try also some other models
import segmentation_models_pytorch as smp
from copy import deepcopy, copy
    
def get_model():
    """Reads `params.yaml` and returns an instance of the requested model"""
    img_size = (PARAMS['image']['width'], PARAMS['image']['height'])
    #return UNet(img_size, starting_channels=PARAMS['model'].get('starting_channels', 1)).to(device)
    poppable_model_params = deepcopy(PARAMS['model'])
    print(poppable_model_params)
    model_class_name = poppable_model_params.pop('class', 'UNet')
    if model_class_name == 'UNet':
        poppable_model_params['img_size'] = img_size
    if '.' in model_class_name:
        module, name = model_class_name.split('.')
        model_class = getattr(globals()[module], name)
    else:
        model_class = globals()[model_class_name]
    model = model_class(**poppable_model_params)
    model.img_size = img_size
    return model
    